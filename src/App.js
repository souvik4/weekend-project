import './App.css';
import WindowWidth from './WindowWidth';

function App() {
  return (
    <div className="App">
       <WindowWidth>
         {width => <div>Window width is: {width}</div>}
       </WindowWidth>
    </div>
  );
}

export default App;
