import {useState, useEffect} from 'react';
import PropTypes from 'prop-types';

const WindowWidth = ({children}) => {
    
  const [windowWidth, setWindowWidth] = useState(window.innerWidth);

  useEffect(() => {   
            
      window.addEventListener('resize', onResize);
      console.log('hii')

      return () => {
        window.removeEventListener('resize', onResize);
        console.log("unmount")  
      }

    },[])
    
    const onResize = () => {
      setWindowWidth(window.innerWidth)
    }
    
    
  return children(windowWidth)
}

WindowWidth.propTypes = {
  children: PropTypes.func.isRequired
}


export default WindowWidth;






// class WindowWidth extends React.Component {
//     propTypes = {
//       children: PropTypes.func.isRequired
//     }
  
//     state = {
//       windowWidth: window.innerWidth,
//     }
  
//     onResize = () => {
//       this.setState({
//         windowWidth: window.innerWidth,
//       })
//     }
  
//     componentDidMount() {
//       window.addEventListener('resize', this.onResize)
//     }
  
//     componentWillUnmount() {
//       window.removeEventListener('resize', this.onResize);
//     }
  
//     render() {
//       return this.props.children(this.state.windowWidth);
//     }
//   }
  
//   // To be used like this:
  
//   const MyComponent = () => {
//     return (
//       <WindowWidth>
//         {width => <div>Window width is: {width}</div>}
//       </WindowWidth>
//     )
//   }